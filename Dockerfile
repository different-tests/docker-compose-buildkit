FROM debian:bullseye-slim

RUN apt-get update && apt-get install -y \
  git && \
  rm -rf /var/lib/apt/lists/* && \
  mkdir -p /root/.ssh && \
  ssh-keyscan gitlab.com > /root/.ssh/known_hosts

RUN --mount=type=ssh git clone git@gitlab.com:different-tests/nunu-api